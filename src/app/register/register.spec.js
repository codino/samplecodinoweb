describe('register controller test', function () {

    var scope, AuthRemoteService, UserRemoteService, AlertsService, $q, $controller, rootScope;


    beforeEach(module('codino.register'));

    beforeEach(inject(function (_$controller_, $rootScope, _$q_, _AuthRemoteService_, _AlertsService_, _UserRemoteService_) {
        $q = _$q_;
        scope = $rootScope.$new();
        rootScope = $rootScope;
        $controller = _$controller_;
        AuthRemoteService = _AuthRemoteService_;
        AlertsService = _AlertsService_;
        UserRemoteService = _UserRemoteService_;
    }));

    function setupController(registerSuccessful) {

        var deferred = $q.defer();
        if (registerSuccessful) {
            deferred.resolve();
        } else {
            deferred.reject({errorMessage: "failure"});
        }
        spyOn(UserRemoteService, 'register').andReturn(deferred.promise);
        spyOn(AuthRemoteService, 'login').andReturn(deferred.promise);
        $controller('RegisterCtrl', {
            $scope: scope,
            UserRemoteService: UserRemoteService,
            AlertsService: AlertsService,
            AuthRemoteService: AuthRemoteService
        });
    }


    it('should register successful', inject(function () {
        //given
        setupController(true);

        //when
        scope.register({username: "test", passowrd: "passowrd"});
        rootScope.$apply();

        //then
        expect(scope.alerts.actionSuccess).toBeTruthy();

    }));

    it('should not register successful', inject(function () {
        //given
        setupController(false);

        //when
        scope.register({username: "test", passowrd: "passowrd"});
        rootScope.$apply();

        //then
        expect(scope.alerts.actionSuccess).toBeFalsy();
        expect(scope.alerts.actionError.show).toBeTruthy();
        expect(scope.alerts.actionError.message).toEqual("failure");

    }));

})
;

