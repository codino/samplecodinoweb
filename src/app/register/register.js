angular.module('codino.register', [
    'ui.router',
    'codino.remote.userRemoteService',
    'codino.utils.alertsService',
    'codino.remote.authRemoteService'
])

    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('register', {
            url: '/register',
            views: {
                "main": {
                    controller: 'RegisterCtrl',
                    templateUrl: 'register/register.tpl.html'
                }
            }
        })
        ;
    }])

    .controller('RegisterCtrl', ['$scope', 'UserRemoteService', 'AlertsService', 'AuthRemoteService', function($scope, UserRemoteService, AlertsService, AuthRemoteService) {

        initUserData();
        $scope.alerts = AlertsService.initAlertData();

        $scope.register = function() {
            $scope.alerts = AlertsService.initAlertData();
            UserRemoteService.register($scope.user).then(function (status) {
                $scope.alerts.actionSuccess = true;
                AuthRemoteService.login($scope.user.username, $scope.user.password);

                }, function(status) {
                AlertsService.setErrorAlert(status.errorMessage);
            });
        };


        function initUserData() {
            $scope.user = {
                username: "",
                password: "",
                email: ""
            };
        }

    }]);

