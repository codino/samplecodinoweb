angular.module('codino.login', [
    'ui.router',
    'codino.remote.authRemoteService',
    'codino.utils.alertsService'
])

    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('login', {
            url: '/login',
            views: {
                "main": {
                    controller: 'LoginCtrl',
                    templateUrl: 'login/login.tpl.html'
                }
            }
        });
    }])

    .controller('LoginCtrl', ['$scope', 'AuthRemoteService', 'AlertsService', function ($scope, AuthRemoteService, AlertsService) {

        initUserData();
        $scope.alerts = AlertsService.initAlertData();

        $scope.login = function () {

            AuthRemoteService.login($scope.user.username, $scope.user.password).then(function () {
                $scope.alerts.actionSuccess = true;

            }, function (reason) {
                AlertsService.setErrorAlert(reason.errorMessage);
            });
        };

        function initUserData() {
            $scope.user = {
                username: "",
                password: ""
            };
        }

    }]);

