describe('login controller test', function () {

    var scope, AuthRemoteService, AlertsService, $q, $controller, rootScope;


    beforeEach(module('codino.login'));

    beforeEach(inject(function (_$controller_, $rootScope, _$q_, _AuthRemoteService_, _AlertsService_) {
        $q = _$q_;
        scope = $rootScope.$new();
        rootScope = $rootScope;
        $controller = _$controller_;
        AuthRemoteService = _AuthRemoteService_;
        AlertsService = _AlertsService_;
    }));

    function setupController(loginSuccessful) {

        var deferred = $q.defer();
        if (loginSuccessful) {
            deferred.resolve();
        } else {
            deferred.reject({errorMsg:"failure"});
        }
        spyOn(AuthRemoteService, 'login').andReturn(deferred.promise);
        $controller('LoginCtrl', {$scope: scope, AuthRemoteService: AuthRemoteService, AlertsService: AlertsService});
    }


    it('should login successful', inject(function () {
        //given
        setupController(true);

        //when
        scope.login("test", "passowrd");
        rootScope.$apply();


        //then
        expect(scope.alerts.actionSuccess).toBeTruthy();

    }));

    it('should not login successful', inject(function () {
        //given
        setupController(false);

        //when
        scope.login("test", "passowrd");
        rootScope.$apply();

        //then
        expect(scope.alerts.actionSuccess).toBeFalsy();
        expect(scope.alerts.actionError.show ).toBeTruthy();

    }));


})
;

