angular.module('codino.user', [
    'ui.router',
    'codino.remote.userRemoteService',
    'codino.remote.authRemoteService'
])

    .config(function config($stateProvider) {
        $stateProvider.state('user', {
            url: '/user',
            views: {
                "main": {
                    controller: 'UserCtrl',
                    templateUrl: 'user/user.tpl.html'
                }
            },
            resolve: {
                userRemoteService: 'UserRemoteService',
                users: function (userRemoteService) {
                    return (userRemoteService.getUsers());
                }
            }
        });
    })


    .controller('UserCtrl', ['$scope', 'users', 'AuthRemoteService', function ($scope, users, AuthRemoteService) {

        $scope.users = users;

        $scope.logout = function(){
            AuthRemoteService.logout();
        };
    }])

;

