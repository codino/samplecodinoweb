describe('user controller test', function () {

  var scope, AuthRemoteService,  $q, $controller, rootScope;


  beforeEach(module('codino.user'));

  beforeEach(inject(function (_$controller_, $rootScope, _$q_, _AuthRemoteService_) {
    $q = _$q_;
    scope = $rootScope.$new();
    rootScope = $rootScope;
    $controller = _$controller_;
    AuthRemoteService = _AuthRemoteService_;
  }));

  function setupController() {

    var deferred = $q.defer();
    deferred.resolve();
    spyOn(AuthRemoteService, 'logout').andReturn(deferred.promise);
    $controller('UserCtrl', {$scope: scope, users: [], AuthRemoteService: AuthRemoteService});
  }


  it('should logout successful', inject(function () {
    //given
    setupController();

    //when
    scope.logout();
    rootScope.$apply();

    //then
    expect(AuthRemoteService.logout).toHaveBeenCalled();

  }));


})
;

