angular.module('codino', [
    'templates-app',
    'templates-common',
    'ui.router',
    'ui.bootstrap',
    'codino.auth.http-auth-interceptor',
    'codino.user',
    'codino.login',
    'codino.register'
])

    .config(['$urlRouterProvider', '$httpProvider', function appConfig($urlRouterProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/user');
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
    }])

    .constant("STATES", {
        "HOME": "user",
        "LOGIN": "login",
        "REGISTER": "register"
    })
    .controller('AppCtrl', ['$scope', '$rootScope', '$state', 'STATES', function ($scope, $rootScope, $state, STATES) {

        var transitionToState = STATES.HOME;
        var transitionToParams = {};

        $rootScope.$on('event:auth-loginRequired', function () {
            $state.go(STATES.LOGIN);
        });

        $rootScope.$on('event:auth-logoutConfirmed', function () {
            transitionToState = STATES.HOME;
            transitionToParams = {};
            $state.go(STATES.LOGIN);
        });

        $rootScope.$on('event:auth-loginConfirmed', function () {
            $state.go(transitionToState, transitionToParams);
            transitionToState = STATES.HOME;
            transitionToParams = {};

        });

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            if (toState.name !== STATES.LOGIN && toState.name !== STATES.REGISTER) {
                transitionToState = toState.name;
                transitionToParams = toParams;
            }
        });
    }])

;

