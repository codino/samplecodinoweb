angular.module('codino.auth.http-auth-interceptor', [
    'codino.notifications.appNotificationsService',
    'codino.auth.userAuthService',
    'ui.router'
])

    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('HttpInterceptor');
    }])
    .factory('HttpInterceptor', ['$q', '$injector', 'UserAuthService', 'AppNotificationsService', function ($q, $injector, UserAuthService, AppNotificationsService) {

        function handleUnauthorized(rejection) {
            var currentState = $injector.get('$state').current.name;

            if (rejection.status === 401 && currentState !== 'login') {
                UserAuthService.removeXAuthToken();
                AppNotificationsService.loginRequired();
                return $q.defer().promise;
            }
            return $q.reject(rejection);
        }

        return {
            responseError: handleUnauthorized
        };
    }]);


