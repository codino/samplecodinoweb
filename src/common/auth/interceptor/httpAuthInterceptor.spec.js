describe('Http interceptor tests', function () {
    var rootScope, $http, $httpBackend;


    beforeEach(module("codino.auth.http-auth-interceptor"));


    beforeEach(inject(function ($rootScope, _$http_, _$httpBackend_) {
        $http = _$http_;
        $httpBackend = _$httpBackend_;
        rootScope = $rootScope;
        spyOn(rootScope, '$broadcast').andCallThrough();

    }));


    it('should broadcast auth-required when unauthenticated response', function () {
        //given
        $httpBackend.expectGET('http://codino.pl/user').respond(401, {});

        //when
        $http.get('http://codino.pl/user');
        $httpBackend.flush();

        //then
        expect(rootScope.$broadcast).toHaveBeenCalledWith('event:auth-loginRequired');
    });

    it('should not broadcast auth-required when 200 OK response', function () {
        //given
        $httpBackend.expectGET('http://codino.pl/user').respond(200, {});

        //when
        $http.get('http://codino.pl/user');
        $httpBackend.flush();

        //then
        expect(rootScope.$broadcast).not.toHaveBeenCalledWith('event:auth-loginRequired');
    });


});