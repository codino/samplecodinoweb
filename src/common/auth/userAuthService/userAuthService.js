angular.module('codino.auth.userAuthService', [
    'ngCookies'
])
    .factory('UserAuthService', ['$cookieStore', function ($cookieStore) {
        var X_AUTH_TOKEN = 'X-Auth-Token';
        var headers = {'X-Auth-Token': getXAuthToken()};

        function saveXAuthToken(xAuthToken) {
            $cookieStore.put(X_AUTH_TOKEN, xAuthToken);
        }

        function getXAuthToken() {
            return $cookieStore.get(X_AUTH_TOKEN);
        }

        return {
            setXAuthToken: function (xAuthToken) {
                headers[X_AUTH_TOKEN] = xAuthToken;
                saveXAuthToken(xAuthToken);
            },

            removeXAuthToken: function () {
                headers[X_AUTH_TOKEN] = '';
                saveXAuthToken('');
            },
            getAuthHeaders: function () {
                return headers;
            }
        };
    }])
;

