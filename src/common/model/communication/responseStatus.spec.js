describe('Response Status tests', function () {
    var ResponseStatus;

    beforeEach(module("codino.model.responseStatus"));

    beforeEach(inject(function (_ResponseStatus_) {
        ResponseStatus = _ResponseStatus_;

    }));

    it('should create valid success response status from json', function () {
        //given
        var jsonServerResponse = {status: "success", objectId:0};

        //when
        var status = ResponseStatus.apiResponseTransformer(jsonServerResponse);

        //then
        expect(status instanceof ResponseStatus).toBeTruthy();
        expect(status.objectId).toEqual(0);
        expect(status.status).toEqual("success");
    });

    it('should create valid error response status from json', function () {
        //given
        var jsonServerResponse = {status: "error", errorMsg:"Login error"};

        //when
        var status = ResponseStatus.apiResponseTransformer(jsonServerResponse);

        //then
        expect(status instanceof ResponseStatus).toBeTruthy();
        expect(status.status).toEqual("error");
        expect(status.errorMessage).toEqual("Login error");
    });

});