angular.module('codino.model.user', [
    'codino.model.detail'
])
    .factory('User', ['Detail', function (Detail) {

        function User(id, username, email, details) {
            this.id = id;
            this.username = username;
            this.email = email;
            this.details = details;

        }

        User.build = function (data) {
            return new User(
                data.id,
                data.username,
                data.email,
                data.details ? data.details.map(Detail.build) : []
            );
        };

        User.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData
                    .map(User.build);
            }
            return User.build(responseData);
        };

        return User;
    }])
;