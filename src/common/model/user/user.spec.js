describe('User tests', function () {
    var User, Detail;

    beforeEach(module("codino.model.user"));

    beforeEach(inject(function (_User_, _Detail_) {
        User = _User_;
        Detail = _Detail_;

    }));

    it('should create valid User with empty Detail list from json', function () {
        //given
        var jsonServerResponse = {username:"Maciek", email:"Maciek@wp.pl", id:0};

        //when
        var user = User.apiResponseTransformer(jsonServerResponse);

        //then
        expect(user instanceof User).toBeTruthy();
        expect(user.id).toEqual(0);
        expect(user.username).toEqual("Maciek");
        expect(user.email).toEqual("Maciek@wp.pl");
        expect(user.details instanceof Array).toBeTruthy();
        expect(user.details.length).toEqual(0);

    });

    it('should create valid user with Detail list from json', function () {
        //given
        var jsonServerResponse = { username:"Maciek", email:"Maciek@wp.pl", details : [{id:0, key:"key_1", value:"value_1" }]};

        //when
        var user = User.apiResponseTransformer(jsonServerResponse);

        //then
        expect(user.details instanceof Array).toBeTruthy();
        expect(user.details.length).toEqual(1);
        expect(user.details[0].id).toEqual(0);
        expect(user.details[0].key).toEqual("key_1");
        expect(user.details[0].value).toEqual("value_1");

    });


});