angular.module('codino.model.detail', [])
    .factory('Detail', [function () {

        function Detail(id, key, value) {
            this.id = id;
            this.key = key;
            this.value = value;
        }

        Detail.build = function (data) {
            return new Detail(
                data.id,
                data.key,
                data.value
            );
        };

        Detail.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData
                    .map(Detail.build);
            }
            return Detail.build(responseData);
        };

        return Detail;
    }])
;