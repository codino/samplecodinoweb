describe('Detail tests', function () {
    var Detail;

    beforeEach(module("codino.model.detail"));

    beforeEach(inject(function (_Detail_) {
        Detail = _Detail_;

    }));

    it('should create valid detail object from json', function () {
        //given
        var jsonServerResponse = {id: 0, key:"key_1", value:"value_1"};

        //when
        var detail = Detail.apiResponseTransformer(jsonServerResponse);

        //then
        expect(detail instanceof Detail).toBeTruthy();
        expect(detail.id).toEqual(0);
        expect(detail.key).toEqual("key_1");
        expect(detail.value).toEqual("value_1");

    });

});