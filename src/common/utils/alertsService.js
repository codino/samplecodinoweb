angular.module('codino.utils.alertsService', [])

    .factory('AlertsService', [function () {

        var alerts = {
            actionSuccess: false,
            actionError: {
                show: false,
                message: ""
            }
        };

        function clearAlertData() {
            alerts.actionSuccess = false;
            alerts.actionError.show = false;
            alerts.actionError.message = "";
        }

        return {

            initAlertData: function () {
                clearAlertData();
                return alerts;
            },
            setErrorAlert: function (message) {
                alerts.actionError.show = true;
                alerts.actionError.message = message;
            }
        };
    }])
;