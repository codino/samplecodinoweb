angular.module('codino.remote.authRemoteService', [
    'codino.remote.api',
    'codino.auth.userAuthService',
    'codino.notifications.appNotificationsService'
])
    .factory('AuthRemoteService', ['UserAuthService', 'API', 'AppNotificationsService', function (UserAuthService, API, AppNotificationsService) {
        return {
            login: function (username, password) {
                return API
                    .unAuthPost('/login', {username: username, password: password})
                    .then(function (response) {
                        UserAuthService.setXAuthToken(response.access_token);
                        AppNotificationsService.loginConfirmed();
                    });
            },
            logout: function () {
                return API
                    .unAuthPost('/logout')
                    .then(function () {
                        UserAuthService.removeXAuthToken();
                        AppNotificationsService.logoutConfirmed();
                    });
            }
        };
    }]);