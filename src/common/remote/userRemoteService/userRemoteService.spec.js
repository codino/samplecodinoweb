describe('User Remote Service tests', function () {
    var UserRemoteService, User, ResponseStatus, $httpBackend;


    beforeEach(module("codino.remote.userRemoteService"));

    beforeEach(inject(function (_UserRemoteService_, _User_, _ResponseStatus_, _$httpBackend_) {
        UserRemoteService = _UserRemoteService_;
        User = _User_;
        ResponseStatus = _ResponseStatus_;
        $httpBackend = _$httpBackend_;
    }));

    it('should register with status success', function () {
        //given
        $httpBackend.expectPOST('api/users', {username: 'user', password: 'password'}).respond(200, {status: 'success'});

        //when
        var status;
        UserRemoteService.register({username: 'user', password: 'password'}).then(function (response) {
            status = response;
        });
        $httpBackend.flush();

        //then
        expect(status instanceof ResponseStatus).toBeTruthy();
        expect(status.status).toEqual("success");
    });

    it('should getUsers successfully', function () {
        //given
        var usersJson = [{id:0, username:"test"}, {id:1, username: "test2"}];
        $httpBackend.expectGET('auth/api/users').respond(200, usersJson);

        //when
        var users;
        UserRemoteService.getUsers().then(function (response) {
            users = response;
        });
        $httpBackend.flush();

        //then
        expect(users instanceof Array).toBeTruthy();
        expect(users.length).toEqual(2);
        expect(users[0] instanceof  User).toBeTruthy();
        expect(users[0].id).toEqual(0);
        expect(users[0].username).toEqual("test");
    });



});