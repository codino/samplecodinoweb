angular.module('codino.remote.userRemoteService', [
    'codino.remote.api',
    'codino.model.responseStatus',
    'codino.model.user'
])
    .factory('UserRemoteService', ['$rootScope', 'API', '$q', 'ResponseStatus', 'User', function ($rootScope, API, $q, ResponseStatus, User) {
        return {
            register: function (userData) {
                var deferred = $q.defer();
                API.unAuthPost('/users', userData)
                    .then(ResponseStatus.apiResponseTransformer)
                    .then(function (status) {
                        if ("success" === status.status) {
                            deferred.resolve(status);
                        } else {
                            deferred.reject(status);
                        }
                    });
                return deferred.promise;
            },
            getUsers: function () {
                var deferred = $q.defer();
                API.get('/users')
                    .then(User.apiResponseTransformer)
                    .then(function (user) {
                        deferred.resolve(user);
                    });
                return deferred.promise;
            }

        };
    }]);
