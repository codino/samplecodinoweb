angular.module('codino.remote.api', [
    'codino.auth.userAuthService'
])
    .factory('API', ['$http', '$q', 'UserAuthService', function ($http, $q, UserAuthService) {

        var authApiPath = 'auth/api';
        var loginApiPath = 'api';

        function createRequest(verb, httpConfing, data, params) {
            var defer = $q.defer();
            verb = verb.toUpperCase();
            httpConfing.method = verb;
            httpConfing.headers = UserAuthService.getAuthHeaders();

            if (verb.match(/POST/)) {
                httpConfing.data = data;
            }

            if (params) {
                httpConfing.params = params;
            }

            $http(httpConfing)
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (data, status) {
                    defer.reject('HTTP Error: ' + status);
                });
            return defer.promise;
        }

        function createAuthHttpConfig(uri) {
            return {url: authApiPath + uri};
        }

        function createHttpConfig(uri) {
            return {url: loginApiPath + uri};
        }

        return {
            get: function (uri, params) {
                return createRequest('get', createAuthHttpConfig(uri), null, params);
            },
            post: function (uri, data, params) {
                return createRequest('post', createAuthHttpConfig(uri), data, params);
            },
            unAuthPost: function (uri, data) {
                return createRequest('post', createHttpConfig(uri), data);
            }
        };

    }]);